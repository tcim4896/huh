var cl = console.log;
var body = document.body;
var mStateMap = {};
var VALUE = 0;

var build = (baseElm, elmSeq)=>{ // engine
    var style = (elm, style)=>{
        for(let prop of Object.keys(style)){
            elm.style[prop] = style[prop];
        }
        return elm;
    };
    var addEventListeners = (elm, eventListeners)=>{ // maybe global
        for(let e of eventListeners){
            elm.addEventListener(e.type, e.fn(elm));
        }
        return elm;
    }
    var r;
    for(let elm of elmSeq) {
        var a = document.createElement(elm.tag);
            a = style(a, elm.style);
            a = addEventListeners(a, elm.eventListeners)
        !r ? 
        r = a :
        r.appendChild(a);
        mStateMap[elm.name] = a;
    }
    return r;
};

function fader(){ // should be object
    let container, knob;
    container = {
        name: 'container',
        tag: 'div',
        style: {
            position: "absolute",
            margin: "10px",
            backgroundColor: "rgba(0,0,0,0.2)",
            border: "1px solid #000",
            width: "10px",
            height: "320px"
        },
        eventListeners: []
    };
    knob = {
        name: 'knob',
        tag: 'div',
        style: {
            position: "absolute",
            left: "-15px",
            top: "50px",
            backgroundColor: "rgba(0,0,0,0.2)",
            border: "1px solid black",
            width: "40px",
            height: "20px",
        },
        eventListeners: [
            {type: 'mousedown', fn: (elm)=>(e)=> { cl("down", e); 
                mStateMap.knob.mouseDown = true;
                mStateMap.knob.e = e;
            }},
            {type: 'mouseup', fn: (elm)=>(e)=> {
                 cl("up", e); 
                 mStateMap.knob.mouseDown = false;
            }},            
        ]
    };

    return [container, knob];
}

cl(fader());
body.addEventListener('mousemove', (e)=> {
    mStateMap = {...mStateMap, ...{y:e.clientY, x:e.clientX}};
    let min, max, knobPos;
    let container = getStyle(mStateMap.container);
    let knob = getStyle(mStateMap.knob);
    
    max = px(container.marginTop) + 
            px(container.height); // cancels out right?

    min = px(container.marginTop);

    knobPos = mStateMap.y;
                
    VALUE = pxSeq([container.marginTop, container.height]) - 
                knobPos - 
                px(knob.height); // <--- 30?

    cl(mStateMap.knob.mouseDown, 
        withinLimits({min, max}, 
            knobPos), 
            min, 
            max, 
            knobPos);

    mStateMap.knob.mouseDown ? 
        withinLimits({min, max}, knobPos) ?
            knob.top = px(mStateMap.y - mStateMap.knob.e.layerX) :
        cl(false) :
    {};

    function withinLimits(limit, value) {
        return value > limit.min && value < limit.max;
    }

    function px(value) {
        return typeof value === "string" ?
            parseInt(value) :
            value + "px";
    }

    function pxSeq(seq){
        return seq.map(val=>px(val)).reduce((a,b)=>a+b,0);
    }

    function getStyle(elm) {
        return elm.style;
    }
})

body.appendChild(build(body, fader()));